# substitute64

Base64 encoder and decoder for non-standard alphabets.
Sometimes the standard encoding is not applicable, for example malware often 
uses custom alphabets and some older programs may have defined their own 
alphabet for use in base64 encoding. Most common tools for decoding and 
encoding base64 do not provide support for custom alphabets.

Since a base64 encoding with custom alphabet is equivalent to encoding with a
standard alphabet and applying a substitution cipher on the result, the name for
this project reflects that.

For use with older programs, the alphabet is most likely just a different order
of the character sets, rather than a fully random set. To accomodate this use
case, the alphabet can be given in an abbreviated format in mere five
characters. For example, the standard alphabet can be abbreviated to `Aa0+/`, or
`Zz9+/`. This works by specifying a single character from uppercase ASCII,
lowercase ASCII, and a digit, with the two extra characters needed separately.
As such for a standard encoding with different extra characters an input like
`Aa0!@` can be used, replacing `+` with `!` and `/` with `@`.

This program prints the output to stdout.
