import argparse
import base64
import string
import sys

debug_mode = False
extra_characters = '+/'

standard_base64_alphabet = string.ascii_uppercase + string.ascii_lowercase + string.digits + extra_characters

def encode(what : bytes, alphabet_bytes : bytes):
    table = bytes.maketrans(bytes(standard_base64_alphabet, 'utf-8'), alphabet_bytes)
    encoded = base64.b64encode(what)
    return encoded.translate(table)


def decode(what : bytes, alphabet_bytes):
    table = bytes.maketrans(alphabet_bytes, bytes(standard_base64_alphabet, 'utf-8'))
    translated = what.translate(table)
    return base64.b64decode(translated)

def parse_alphabet(raw_string) -> bytes:
    if len(raw_string) == len(standard_base64_alphabet):
        return bytes(raw_string, 'utf-8')
    if len(raw_string) != 5:
        print('The alphabet needs to be either complete or exactly 5 characters long sequence of representatives.')
        sys.exit(1)

    alphabet = ''
    for representative in raw_string:
        if representative in string.ascii_uppercase:
            alphabet = alphabet + string.ascii_uppercase
        elif representative in string.ascii_lowercase:
            alphabet = alphabet + string.ascii_lowercase
        elif representative in string.digits:
            alphabet = alphabet + string.digits
        else:
            alphabet = alphabet + representative

    if len(alphabet) != len(standard_base64_alphabet):
        print('Abbreviated alphabet requires uppercase, lowercase, digit and two special characters to be a valid shortening.', file=sys.stderr)

    return bytes(alphabet, 'utf-8')



def main():
    parser = argparse.ArgumentParser(
            description='Base64 encoder and decoder for nonstandard alphabets',
            epilog='The results are printed to standard out.'
            )
    parser.add_argument('-v', '--version', action='version', help='print version string', version='%(prog)s 0.1')

    inputs_group = parser.add_argument_group('inputs')
    inputs_group.add_argument('-i', '--input', required=True, help='input string to decode/encode')
    inputs_group.add_argument('-a', '--alphabet', default=standard_base64_alphabet, help=
                              'substitution alphabet to use when decoding/encoding. If the alphabet is '
                              'just a reordering of the sequences, a single representative can be used '
                              'for digits, lower, and uppercase letters: -a Aa0+/ represents the '
                              'standard alphabet and -a 0Aa+/ would have the digits first. Input must '
                              'either be 5 or 64 characters long, that is either enter'
                              ' an abbreviated string, or the full alphabet.'
                              )

    action_group = parser.add_argument_group('actions')
    action_to_do = action_group.add_mutually_exclusive_group(required=True)
    action_to_do.add_argument('-e', '--encode', action='store_true', help='encode input using provided alphabet')
    action_to_do.add_argument('-d', '--decode', action='store_true', help='decode input using provided alphabet')

    output_control_group = parser.add_argument_group('output controls')
    output_control_group.add_argument('-n', '--nonewline', action='store_true', help='disable printing newline at the end of the output')
    output_control_group.add_argument('--debug', action='store_true', help='may provide debug information')

    if len(sys.argv) == 1:
        parser.print_help(sys.stderr)
        sys.exit(1)

    arguments = parser.parse_args()
    if arguments.debug:
        print('DEBUG: arguments:', arguments)
        debug_mode = True

    alphabet_bytes = parse_alphabet(arguments.alphabet)
    if arguments.encode:
        output = encode(bytes(arguments.input, 'utf-8'), alphabet_bytes)
        sys.stdout.buffer.write(output)
    elif arguments.decode:
        output = decode(bytes(arguments.input, 'utf-8'), alphabet_bytes)
        sys.stdout.buffer.write(output)
    else:
        print('Set either encode or decode flags.', file=sys.stderr)
        parser.print_help(sys.stderr)
        sys.exit(1)

    if not arguments.nonewline:
        print()

if __name__ == "__main__":
    main()
