import unittest
import substitute64

standard_alphabet = b'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
reverse_alphabet = b'/+9876543210zyxwvutsrqponmlkjihgfedcbaZYXWVUTSRQPONMLKJIHGFEDCBA'
odd_order_alphabet = b'+abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ/'
swapped_extra_alphabet = b'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@'

class TestSubstitute64(unittest.TestCase):
    def test_standard_alphabet(self):
        string = 'Hello! This is a test string!'
        encoded = substitute64.encode(bytes(string, 'utf-8'), standard_alphabet)
        self.assertEqual(encoded, b'SGVsbG8hIFRoaXMgaXMgYSB0ZXN0IHN0cmluZyE=')

        decoded = substitute64.decode(encoded, standard_alphabet)
        self.assertEqual(decoded.decode(), string)

    def test_reversed_alphabet(self):
        string = 'Hello! This is a test string!'
        encoded = substitute64.encode(bytes(string, 'utf-8'), reverse_alphabet)
        self.assertEqual(encoded, b't5qTk5De36uXlozflozfnt+LmoyL34yLjZaRmN7=')

        decoded = substitute64.decode(encoded, reverse_alphabet)
        self.assertEqual(decoded.decode(), string)

    def test_parse_alphabet_standard(self):
        alphabet = substitute64.parse_alphabet(standard_alphabet.decode())
        self.assertEqual(alphabet, standard_alphabet)

    def test_parse_alphabet_reverse(self):
        alphabet = substitute64.parse_alphabet(reverse_alphabet.decode())
        self.assertEqual(alphabet, reverse_alphabet)

    def test_parse_alphabet_abbreviated(self):
        standard_abbreviated = substitute64.parse_alphabet('Aa0+/')
        self.assertEqual(standard_abbreviated, standard_alphabet)

        standard_abbreviated2 = substitute64.parse_alphabet('Zz9+/')
        self.assertEqual(standard_abbreviated2, standard_alphabet)

        different_extra_abbreviated = substitute64.parse_alphabet('Aa0!@')
        self.assertEqual(different_extra_abbreviated, swapped_extra_alphabet)

        odd_order_abbreviated = substitute64.parse_alphabet('+a0A/')
        self.assertEqual(odd_order_abbreviated, odd_order_alphabet)



if __name__ == "__main__":
    unittest.main()
